package com.yakovliam.spaceshop;

import com.yakovliam.yakocoreapi.YakoPlugin;
import lombok.Getter;

public class SpaceShop extends YakoPlugin {

    @Getter
    private static SpaceShop instance;

    @Override
    public void onLoad() {
        // set instance
        instance = this;
    }

    @Override
    public void onEnable() {
        // Plugin startup logic

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
